package main

import (
	"flag"
	"gitlab.com/gzavodov/scrambler/internal/chat/tui"
	chatclient "gitlab.com/gzavodov/scrambler/pkg/chat/client"
	"log"

	chat "gitlab.com/gzavodov/scrambler/internal/chat/tcp"
)

func main() {
	address := flag.String("server", "localhost:3333", "Which server to connect to")
	configFilePath := flag.String("config", "", "Path to configuration file")

	flag.Parse()

	if *configFilePath == "" {
		*configFilePath = "./config/client.config.development.json"
	}

	cfg := &chatclient.Config{}
	if err := cfg.LoadFromFile(*configFilePath); err != nil {
		log.Fatal(err)
	}

	client := chat.NewClient(cfg)
	if err := client.Dial(*address); err != nil {
		log.Fatal(err)
	}

	defer client.Close()

	// start the client to listen for incoming message
	go client.Start()

	tui.StartUi(client)
}
