package main

import (
	chat "gitlab.com/gzavodov/scrambler/internal/chat/tcp"
)

func main() {
	s := chat.NewServer()
	s.Listen(":3333")

	// start the server
	s.Start()
}
