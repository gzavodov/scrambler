package server

type Server interface {
	Start()
	Close()

	Listen(address string) error
	Broadcast(command interface{}) error
}
