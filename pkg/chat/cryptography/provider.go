package cryptography

type Provider interface {
	GetAlgorithmName() string
	GetInfo() string

	IsInitialized() bool
	Initialize() error

	IsActive() bool
	SetActive(active bool)

	Encode(input []byte) ([]byte, error)
	Decode(input []byte) ([]byte, error)
}
