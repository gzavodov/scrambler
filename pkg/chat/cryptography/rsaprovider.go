package cryptography

import (
	"bytes"
	"crypto/sha256"
	"fmt"
	"gitlab.com/gzavodov/scrambler/pkg/cryptography/rsa"
	"hash"
)

const RsaDefaultBitCount = 512

type RsaProviderConfig struct {
	BitCount   int  `json:"bit_count"`
	EnableOAEP bool `json:"enable_oaep"`
}

func NewRsaProviderConfig(bitCount int, enableOAEP bool) *RsaProviderConfig {
	return &RsaProviderConfig{BitCount: bitCount, EnableOAEP: enableOAEP}
}

func (c *RsaProviderConfig) GetAlgorithmName() string {
	return CryptoAlgorithmRSA
}

func (c *RsaProviderConfig) GetBitCount() int {
	if c.BitCount <= 0 {
		return RsaDefaultBitCount
	}

	return c.BitCount
}

func (c *RsaProviderConfig) GetEnableOAEP() bool {
	return c.EnableOAEP
}

type RsaProvider struct {
	cfg        *RsaProviderConfig
	readBuffer *bytes.Buffer

	isInitialized bool
	isActive      bool

	algorithm         *rsa.Algorithm
	oppositePublicKey *rsa.PublicKey

	enableOAEP bool
	hashing    hash.Hash
}

func NewRsaProvider(cfg *RsaProviderConfig) *RsaProvider {
	return &RsaProvider{
		cfg:        cfg,
		algorithm:  rsa.NewAlgorithm(cfg.GetBitCount()),
		readBuffer: &bytes.Buffer{},
		enableOAEP: cfg.GetEnableOAEP(),
		hashing:    sha256.New(),
	}
}

func (p *RsaProvider) GetAlgorithmName() string {
	return CryptoAlgorithmRSA
}

func (p *RsaProvider) GetInfo() string {
	status := "Disabled"
	if p.isActive {
		status = "Enabled"
	}

	oaepStatus := "Disabled"
	if p.enableOAEP {
		oaepStatus = "Enabled"
	}

	return fmt.Sprintf("%s %d-bit (OAEP: %s) Status: %s", CryptoAlgorithmRSA, p.algorithm.BitCount, oaepStatus, status)
}

func (p *RsaProvider) IsInitialized() bool {
	return p.isInitialized
}

func (p *RsaProvider) Initialize() error {
	if p.isInitialized {
		return nil
	}

	_, err := p.algorithm.GenerateKeys(nil)
	if err == nil {
		p.isInitialized = true
	}

	return err
}

func (p *RsaProvider) GetBitCount() int {
	return p.algorithm.BitCount
}

func (p *RsaProvider) GetEnableOAEP() bool {
	return p.enableOAEP
}

func (p *RsaProvider) GetPublicKey() *rsa.PublicKey {
	return p.algorithm.Keys.Public
}

func (p *RsaProvider) GetOppositePublicKey() *rsa.PublicKey {
	return p.oppositePublicKey
}

func (p *RsaProvider) SetOppositePublicKey(publicKey *rsa.PublicKey) {
	p.oppositePublicKey = publicKey
}

func (p *RsaProvider) IsActive() bool {
	return p.isActive
}

func (p *RsaProvider) SetActive(active bool) {
	p.isActive = active
}

func (p *RsaProvider) Encode(input []byte) ([]byte, error) {
	if !p.isActive {
		return input, nil
	}

	if p.enableOAEP {
		return p.algorithm.EncodeOAEP(input, p.oppositePublicKey, p.hashing)
	}

	return p.algorithm.Encode(input, p.oppositePublicKey)
}

func (p *RsaProvider) Decode(input []byte) ([]byte, error) {
	if !p.isActive {
		return input, nil
	}

	if p.enableOAEP {
		return p.algorithm.DecodeOAEP(input, p.algorithm.Keys.Private, p.hashing)
	}

	return p.algorithm.Decode(input, p.algorithm.Keys.Private)
}
