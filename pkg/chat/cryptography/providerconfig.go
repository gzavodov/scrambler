package cryptography

type ProviderConfig interface {
	GetAlgorithmName() string
	GetBitCount() int
}
