package cryptography

import (
	"errors"
)

const CryptoAlgorithmRSA = "RSA"

var (
	ErrUnsupportedProvider          = errors.New("cryptography provider is not supported")
	ErrInvalidProviderConfiguration = errors.New("cryptography provider config is not valid")
)

func NewProvider(cfg ProviderConfig) (Provider, error) {
	switch cfg.GetAlgorithmName() {
	case CryptoAlgorithmRSA:
		cfg, ok := cfg.(*RsaProviderConfig)
		if !ok {
			return nil, ErrInvalidProviderConfiguration
		}

		return NewRsaProvider(cfg), nil
	}

	return nil, ErrUnsupportedProvider
}
