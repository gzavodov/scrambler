package client

import (
	"encoding/json"
	"fmt"
	"gitlab.com/gzavodov/scrambler/pkg/chat/cryptography"
	"io/ioutil"
)

type Config struct {
	CryptographyScheme string                          `json:"cryptography_scheme"`
	Rsa                *cryptography.RsaProviderConfig `json:"rsa"`
}

// LoadFromFile read configuration from file
func (c *Config) LoadFromFile(filePath string) error {
	configFile, err := ioutil.ReadFile(filePath)
	if err != nil {
		return fmt.Errorf("could not read configuration file: %w", err)
	}

	if json.Unmarshal(configFile, c) != nil {
		return fmt.Errorf("could not internalize configuration file data: %w", err)
	}

	return nil
}
