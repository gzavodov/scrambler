package client

import "gitlab.com/gzavodov/scrambler/pkg/chat/protocol"

type Subscriber interface {
	NotifyStateChanged(observer Observer)
}

type Observer interface {
	AddSubscriber(subscriber Subscriber)
}

type Client interface {
	Dial(address string) error
	Start()
	Close()
	Send(command interface{}) error
	SetName(name string) error
	SendMessage(message string) error
	Error() chan error
	Incoming() chan protocol.MessageCommand

	AddSubscriber(subscriber Subscriber)

	GetCryptographyInfo() string
}
