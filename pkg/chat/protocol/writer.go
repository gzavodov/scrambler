package protocol

import (
	"fmt"
	"io"
	"log"
)

type CryptoEncoder interface {
	Encode([]byte) ([]byte, error)
}

type CommandWriter struct {
	writer  io.Writer
	encoder CryptoEncoder
}

func NewCommandWriter(writer io.Writer, encoder CryptoEncoder) *CommandWriter {
	return &CommandWriter{
		writer:  writer,
		encoder: encoder,
	}
}

func (w *CommandWriter) GetEncoder() CryptoEncoder {
	return w.encoder
}

func (w *CommandWriter) SetEncoder(encoder CryptoEncoder) {
	w.encoder = encoder
}

func (w *CommandWriter) writeCommand(msg string) error {
	if w.encoder == nil {
		_, err := w.writer.Write(append([]byte(msg), CommandDelimiter...))
		return err
	}

	encoded, err := w.encoder.Encode([]byte(msg))
	if err != nil {
		return err
	}

	_, err = w.writer.Write(append(encoded, CommandDelimiter...))
	return err
}

func (w *CommandWriter) Write(command interface{}) error {
	switch v := command.(type) {
	case SendCommand:
		if err := w.writeCommand(fmt.Sprintf("SEND %v", v.Message)); err != nil {
			return err
		}
	case MessageCommand:
		if err := w.writeCommand(fmt.Sprintf("MESSAGE %v %v", v.Name, v.Message)); err != nil {
			return err
		}
	case NameCommand:
		if err := w.writeCommand(fmt.Sprintf("NAME %v", v.Name)); err != nil {
			return err
		}
	case *SecureConnectionRequest:
		serializedData, err := v.AlgorithmData.Serialize()
		if err != nil {
			return err
		}

		if err = w.writeCommand(fmt.Sprintf("%s %s %s", v.GetCommandName(), v.AlgorithmName, string(serializedData))); err != nil {
			return err
		}
	case *SecureConnectionAcknowledgement:
		serializedData, err := v.AlgorithmData.Serialize()
		if err != nil {
			return err
		}

		if err = w.writeCommand(fmt.Sprintf("%s %s %s", v.GetCommandName(), v.AlgorithmName, string(serializedData))); err != nil {
			return err
		}
	default:
		log.Printf("unsupported command: %v", v)
		return ErrUnsupportedCommand
	}

	return nil
}
