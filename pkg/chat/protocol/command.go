package protocol

import (
	"errors"
	"gitlab.com/gzavodov/scrambler/pkg/chat/cryptography"
	"gitlab.com/gzavodov/scrambler/pkg/cryptography/rsa"
)

const (
	CommandSecureConnectionRequest         = "SECURE_CONNECTION_REQUEST"
	CommandSecureConnectionAcknowledgement = "SECURE_CONNECTION_ACKNOWLEDGEMENT"
)

var (
	ErrUnsupportedCommand = errors.New("command is not supported")

	// CommandDelimiter разделитель команд (используем UTF-32 BE BOM)
	CommandDelimiter = []byte("\x00\x00\xfe\xff")

	// CommandFieldDelimiter разделитель полей в команде
	CommandFieldDelimiter = []byte{' '}
)

// SendCommand is used for sending new message from client
type SendCommand struct {
	Message string
}

// NameCommand is used for setting client display name
type NameCommand struct {
	Name string
}

// SecureConnectionRequest команда запрашивает протокол шифрования канала
type SecureConnectionRequest struct {
	AlgorithmName string
	AlgorithmData CryptoAlgorithmData
}

func (c *SecureConnectionRequest) GetCommandName() string {
	return CommandSecureConnectionRequest
}

func NewRSAConnectionRequest(publicKey *rsa.PublicKey, bitCount int, enableOAEP bool) *SecureConnectionRequest {
	return &SecureConnectionRequest{
		AlgorithmName: cryptography.CryptoAlgorithmRSA,
		AlgorithmData: NewRSAProtocolData(publicKey, bitCount, enableOAEP),
	}
}

// SecureConnectionAcknowledgement команда подтверждает протокол шифрования канала
type SecureConnectionAcknowledgement struct {
	AlgorithmName string
	AlgorithmData CryptoAlgorithmData
}

func (c *SecureConnectionAcknowledgement) GetCommandName() string {
	return CommandSecureConnectionAcknowledgement
}

func NewRSAConnectionAcknowledgement(publicKey *rsa.PublicKey, bitCount int, enableOAEP bool) *SecureConnectionAcknowledgement {
	return &SecureConnectionAcknowledgement{
		AlgorithmName: cryptography.CryptoAlgorithmRSA,
		AlgorithmData: NewRSAProtocolData(publicKey, bitCount, enableOAEP),
	}
}

// MessageCommand is used for notifying new messages
type MessageCommand struct {
	Name    string
	Message string
}
