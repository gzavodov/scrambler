package protocol

import (
	"encoding/json"
	"errors"
	"gitlab.com/gzavodov/scrambler/pkg/chat/cryptography"
	"gitlab.com/gzavodov/scrambler/pkg/cryptography/rsa"
)

var (
	ErrUnsupportedSecurityProtocol = errors.New("security protocol is not supported")
)

type CryptoAlgorithmData interface {
	Serialize() ([]byte, error)
	Deserialize([]byte) error
}

func NewRSAProtocolData(publicKey *rsa.PublicKey, bitCount int, enableOAEP bool) *RSAProtocolData {
	return &RSAProtocolData{PublicKey: publicKey, BitCount: bitCount, EnableOAEP: enableOAEP}
}

type RSAProtocolData struct {
	PublicKey  *rsa.PublicKey `json:"public_key"`
	BitCount   int            `json:"bit_count"`
	EnableOAEP bool           `json:"enable_oaep"`
}

func (d *RSAProtocolData) Serialize() ([]byte, error) {
	return json.Marshal(d)
}

func (d *RSAProtocolData) Deserialize(data []byte) error {
	return json.Unmarshal(data, d)
}

func NewSecurityProtocolData(name string) (CryptoAlgorithmData, error) {
	switch name {
	case cryptography.CryptoAlgorithmRSA:
		return NewRSAProtocolData(nil, 0, false), nil
	}

	return nil, ErrUnsupportedSecurityProtocol
}
