package protocol

import (
	"bufio"
	"bytes"
	"errors"
	"io"
	"log"
)

var (
	ErrConnectionClosed = errors.New("connection is closed")
)

type CryptoDecoder interface {
	Decode([]byte) ([]byte, error)
}

type CommandReader struct {
	reader  io.Reader
	scanner *bufio.Scanner
	decoder CryptoDecoder
}

func NewCommandReader(reader io.Reader, decoder CryptoDecoder) *CommandReader {
	scanner := bufio.NewScanner(bufio.NewReader(reader))
	scanner.Split(ScanCommandDelimiter)

	return &CommandReader{
		reader:  reader,
		scanner: scanner,
		decoder: decoder,
	}
}

func (r *CommandReader) GetDecoder() CryptoDecoder {
	return r.decoder
}

func (r *CommandReader) SetDecoder(decoder CryptoDecoder) {
	r.decoder = decoder
}

func (r *CommandReader) Read() (interface{}, error) {
	if !r.scanner.Scan() {
		err := r.scanner.Err()
		if err == nil {
			err = ErrConnectionClosed
		}

		return nil, err
	}

	frame := r.scanner.Bytes()
	if r.decoder != nil {
		decoded, err := r.decoder.Decode(frame)
		if err != nil {
			return nil, err
		}

		frame = decoded
	}

	parts := bytes.SplitN(frame, CommandFieldDelimiter, 2)
	switch string(parts[0]) {
	case "MESSAGE":
		msgParts := bytes.SplitN(parts[1], CommandFieldDelimiter, 2)
		return MessageCommand{
			string(msgParts[0]),
			string(msgParts[1]),
		}, nil
	case "SEND":
		return SendCommand{string(parts[1])}, nil
	case "NAME":
		return NameCommand{string(parts[1])}, nil
	case CommandSecureConnectionRequest:
		var err error

		reqParts := bytes.SplitN(parts[1], CommandFieldDelimiter, 2)

		var algorithmData CryptoAlgorithmData
		if algorithmData, err = NewSecurityProtocolData(string(reqParts[0])); err != nil {
			return nil, err
		}

		if err = algorithmData.Deserialize(reqParts[1]); err != nil {
			return nil, err
		}

		return SecureConnectionRequest{AlgorithmName: string(reqParts[0]), AlgorithmData: algorithmData}, nil
	case CommandSecureConnectionAcknowledgement:
		var err error

		reqParts := bytes.SplitN(parts[1], CommandFieldDelimiter, 2)

		var data CryptoAlgorithmData
		if data, err = NewSecurityProtocolData(string(reqParts[0])); err != nil {
			return nil, err
		}

		if err = data.Deserialize(reqParts[1]); err != nil {
			return nil, err
		}

		return SecureConnectionAcknowledgement{AlgorithmName: string(reqParts[0]), AlgorithmData: data}, nil
	default:
		log.Printf("unsupported command: %s\n", parts[0])
	}

	return nil, ErrUnsupportedCommand
}

func (r *CommandReader) ReadAll() ([]interface{}, error) {
	var commands []interface{}

	for {
		command, err := r.Read()

		if command != nil {
			commands = append(commands, command)
		}

		if errors.Is(err, ErrConnectionClosed) {
			break
		} else if err != nil {
			return commands, err
		}
	}

	return commands, nil
}

func ScanCommandDelimiter(data []byte, atEOF bool) (advance int, token []byte, err error) {
	if atEOF && len(data) == 0 {
		return 0, nil, nil
	}
	if i := bytes.Index(data, CommandDelimiter); i >= 0 {
		return i + len(CommandDelimiter), data[0:i], nil
	}

	if atEOF {
		return len(data), data, nil
	}

	return 0, nil, nil
}

//func trimCommandDelimiter(data []byte) []byte {
//	if len(data) > 0 && data[len(data)-1] == CommandDelimiter[0] {
//		return data[0 : len(data)-1]
//	}
//	return data
//}
