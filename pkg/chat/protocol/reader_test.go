package protocol

import (
	"reflect"
	"strings"
	"testing"
)

func TestCommandReader(t *testing.T) {
	tests := []struct {
		input   string
		results []interface{}
	}{
		{
			"SEND test\r\n",
			[]interface{}{
				SendCommand{"test"},
			},
		},
		{
			"MESSAGE user1 hello\r\nMESSAGE user2 world\r\n",
			[]interface{}{
				MessageCommand{"user1", "hello"},
				MessageCommand{"user2", "world"},
			},
		},
	}

	for _, test := range tests {
		reader := NewCommandReader(strings.NewReader(test.input), nil)
		results, err := reader.ReadAll()

		t.Log(results)

		if err != nil {
			t.Errorf("Unable to read command, error %v", err)
		} else if !reflect.DeepEqual(results, test.results) {
			t.Errorf("Command output is not the same: %v %v", results, test.results)
		}
	}
}
