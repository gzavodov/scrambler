package rsa

import (
	"bytes"
	"crypto/rand"
	"crypto/subtle"
	"errors"
	"fmt"
	"hash"
	"io"
	"math/big"
)

const (
	// MersenneNumber число Мерсе́нна (2^17-1). Штатная реализация использует 65537.
	MersenneNumber uint32 = 131071
	PrimeCount     uint16 = 2
)

var ErrPublicKeyNotDefined = errors.New("public key is not defined")
var ErrPrivateKeyNotDefined = errors.New("private key is not defined")

// ErrMessageTooLong is returned when attempting to encrypt a message which is
// too large for the size of the public key.
var ErrMessageTooLong = errors.New("message too long for public key size")
var ErrDecryption = errors.New("decryption error")

//UTF-8 BOM
var split = []byte("\xef\xbb\xbf")

// PublicKey Открытый ключ
type PublicKey struct {
	// N Модуль
	N *big.Int
	// E Экспонента открытого ключа
	E uint32
}

func (k *PublicKey) String() string {
	return fmt.Sprintf("N: %d E: %d", k.N, k.E)
}

// Size возвращает длину Модуля (N) в байтах
func (k *PublicKey) Size() int {
	return (k.N.BitLen() + 7) / 8
}

// PrivateKey закрытый ключ
type PrivateKey struct {
	// N Модуль
	N *big.Int
	// D Экспонента закрытого ключа
	D *big.Int

	// PrimeNumbers Исходные (случайно сгенерированные) простые числа
	PrimeNumbers []*big.Int
}

// Size возвращает длину Модуля (N) в байтах
func (k *PrivateKey) Size() int {
	return (k.N.BitLen() + 7) / 8
}

// KeySet набор ключей (открытый + закрытый)
type KeySet struct {
	Public  *PublicKey
	Private *PrivateKey
}

type Algorithm struct {
	BitCount int
	Keys     *KeySet
}

func NewAlgorithm(bitCount int) *Algorithm {
	return &Algorithm{BitCount: bitCount}
}

func (a *Algorithm) ensureValuesAreUnique(values []*big.Int) bool {
	count := len(values)
	for i := 0; i < count; i++ {
		for j := i + 1; j < count; j++ {
			if values[i].Cmp(values[j]) == 0 {
				return false
			}
		}
	}

	return true
}

func (a *Algorithm) generatePrimeNumbers(count int, random io.Reader) ([]*big.Int, error) {
	numbers := make([]*big.Int, count)
	bitCount := a.BitCount
	for i := 0; i < count; i++ {
		number, err := rand.Prime(random, bitCount/(count-i))
		if err != nil {
			return nil, err
		}

		numbers[i] = number
		bitCount -= number.BitLen()
	}

	return numbers, nil
}

func (a *Algorithm) GenerateKeys(random io.Reader) (*KeySet, error) {
	if random == nil {
		random = rand.Reader
	}

	keySet := &KeySet{Public: &PublicKey{E: MersenneNumber}, Private: &PrivateKey{}}

	var (
		primeNumbers []*big.Int
		err          error
	)

	for {
		primeNumbers, err = a.generatePrimeNumbers(int(PrimeCount), random)
		if err != nil {
			return nil, err
		}

		if !a.ensureValuesAreUnique(primeNumbers) {
			continue
		}

		// Произведение (модуль)
		module := new(big.Int).SetInt64(1)
		// Значение фу-и Эйлера для модуля
		totient := new(big.Int).SetInt64(1)

		for _, number := range primeNumbers {
			module.Mul(module, number)
			totient.Mul(
				totient,
				new(big.Int).Sub(number, new(big.Int).SetInt64(1)),
			)
		}

		e := big.NewInt(int64(keySet.Public.E))
		d := new(big.Int)

		/*
		 * в мультипликативно обратное к числу e по модулю φ(n)
		 * d = invmod(e, φ(n))
		 * при этом d⋅e ≡ 1(mod φ(n))
		 */

		if d.ModInverse(e, totient) == nil {
			continue
		}

		keySet.Private.D = d
		keySet.Private.PrimeNumbers = primeNumbers
		keySet.Private.N = module

		keySet.Public.N = module

		break
	}

	a.Keys = keySet

	return keySet, nil
}

func (a *Algorithm) encrypt(input []byte, key *PublicKey) *big.Int {
	return new(big.Int).Exp(new(big.Int).SetBytes(input), big.NewInt(int64(key.E)), key.N)
}

func (a *Algorithm) encryptOAEP(hash hash.Hash, random io.Reader, key *PublicKey, number *big.Int, label []byte) ([]byte, error) {
	hash.Reset()

	k := key.Size()
	b := number.Bytes()
	if len(b) > k-2*hash.Size()-2 {
		return nil, ErrMessageTooLong
	}

	hash.Write(label)
	lHash := hash.Sum(nil)
	hash.Reset()

	em := make([]byte, k)
	seed := em[1 : 1+hash.Size()]
	db := em[1+hash.Size():]

	copy(db[0:hash.Size()], lHash)
	db[len(db)-len(b)-1] = 1
	copy(db[len(db)-len(b):], b)

	_, err := io.ReadFull(random, seed)
	if err != nil {
		return nil, err
	}

	mgf1XOR(db, hash, seed)
	mgf1XOR(seed, hash, db)

	c := a.encrypt(em, key)
	out := c.Bytes()

	if len(out) < k {
		// If the output is too small, we need to left-pad with zeros.
		t := make([]byte, k)
		copy(t[k-len(out):], out)
		out = t
	}

	return out, nil
}

func (a *Algorithm) Encode(input []byte, key *PublicKey) ([]byte, error) {
	//log.Printf("alg before encode: %s\n", string(input))
	if key == nil {
		//log.Println("alg use inner key")
		key = a.Keys.Public
	}

	if key == nil {
		return nil, ErrPublicKeyNotDefined
	}

	//return a.encrypt(new(big.Int).SetBytes(input), key).Bytes(), nil

	output := make([][]byte, len(input))
	for i, b := range input {
		output[i] = a.encrypt(
			[]byte{b},
			key,
		).Bytes()
	}

	//log.Printf("alg: after encode: %v\n", bytes.Join(output, split))
	return bytes.Join(output, split), nil
}

func (a *Algorithm) EncodeOAEP(input []byte, key *PublicKey, hash hash.Hash) ([]byte, error) {
	//log.Printf("alg: before encode OAEP: %s\n", string(input))
	if key == nil {
		//log.Println("alg use inner key")
		key = a.Keys.Public
	}

	if key == nil {
		return nil, ErrPublicKeyNotDefined
	}

	//return a.encrypt(new(big.Int).SetBytes(input), key).Bytes(), nil

	random := rand.Reader

	output := make([][]byte, len(input))
	for i, b := range input {
		results, err := a.encryptOAEP(hash,
			random,
			key,
			new(big.Int).SetBytes([]byte{b}),
			nil,
		)

		if err != nil {
			return nil, err
		}

		output[i] = results
	}

	//log.Printf("alg: after encode OAEP: %v\n", bytes.Join(output, split))
	return bytes.Join(output, split), nil
}

func (a *Algorithm) decrypt(input []byte, key *PrivateKey) *big.Int {
	return new(big.Int).Exp(new(big.Int).SetBytes(input), key.D, key.N)
}

func (a *Algorithm) decryptOAEP(input []byte, key *PrivateKey, hash hash.Hash, label []byte) ([]byte, error) {
	k := key.Size()
	if len(input) > k ||
		k < hash.Size()*2+2 {
		return nil, ErrDecryption
	}

	m := a.decrypt(input, key)

	hash.Reset()

	hash.Write(label)
	lHash := hash.Sum(nil)

	hash.Reset()

	em := leftPad(m.Bytes(), k)

	firstByteIsZero := subtle.ConstantTimeByteEq(em[0], 0)

	seed := em[1 : hash.Size()+1]
	db := em[hash.Size()+1:]

	mgf1XOR(seed, hash, db)
	mgf1XOR(db, hash, seed)

	lHash2 := db[0:hash.Size()]

	lHash2Good := subtle.ConstantTimeCompare(lHash, lHash2)

	var lookingForIndex, index, invalid int
	lookingForIndex = 1
	rest := db[hash.Size():]

	for i := 0; i < len(rest); i++ {
		equals0 := subtle.ConstantTimeByteEq(rest[i], 0)
		equals1 := subtle.ConstantTimeByteEq(rest[i], 1)
		index = subtle.ConstantTimeSelect(lookingForIndex&equals1, i, index)
		lookingForIndex = subtle.ConstantTimeSelect(equals1, 0, lookingForIndex)
		invalid = subtle.ConstantTimeSelect(lookingForIndex&^equals0, 1, invalid)
	}

	if firstByteIsZero&lHash2Good&^invalid&^lookingForIndex != 1 {
		return nil, ErrDecryption
	}

	return rest[index+1:], nil
}

func (a *Algorithm) Decode(input []byte, key *PrivateKey) ([]byte, error) {
	if key == nil {
		key = a.Keys.Private
	}

	if key == nil {
		return nil, ErrPrivateKeyNotDefined
	}

	//return a.decrypt(new(big.Int).SetBytes(input), key).Bytes(), nil

	output := make([][]byte, 0)
	for _, b := range bytes.Split(input, split) {
		result := a.decrypt(b, key)
		output = append(output, result.Bytes())
	}

	return bytes.Join(output, nil), nil
}

func (a *Algorithm) DecodeOAEP(input []byte, key *PrivateKey, hash hash.Hash) ([]byte, error) {
	if key == nil {
		key = a.Keys.Private
	}

	if key == nil {
		return nil, ErrPrivateKeyNotDefined
	}

	//return a.decrypt(new(big.Int).SetBytes(input), key).Bytes(), nil

	output := make([][]byte, 0)
	for _, b := range bytes.Split(input, split) {
		results, err := a.decryptOAEP(b, key, hash, nil)
		if err != nil {
			return nil, err
		}

		output = append(output, results)
	}

	return bytes.Join(output, nil), nil
}

// MGF1 функция определённая PKCS#1 v2.1.
func mgf1XOR(out []byte, hash hash.Hash, seed []byte) {
	var (
		counter [4]byte
		digest  []byte
	)

	done := 0
	for done < len(out) {
		hash.Write(seed)
		hash.Write(counter[0:4])
		digest = hash.Sum(digest[:0])
		hash.Reset()

		for i := 0; i < len(digest) && done < len(out); i++ {
			out[done] ^= digest[i]
			done++
		}
		incCounter(&counter)
	}
}

func incCounter(c *[4]byte) {
	if c[3]++; c[3] != 0 {
		return
	}
	if c[2]++; c[2] != 0 {
		return
	}
	if c[1]++; c[1] != 0 {
		return
	}
	c[0]++
}

func leftPad(input []byte, size int) (out []byte) {
	n := len(input)
	if n > size {
		n = size
	}
	out = make([]byte, size)
	copy(out[len(out)-n:], input)
	return
}
