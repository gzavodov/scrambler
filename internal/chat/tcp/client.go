package tcp

import (
	"errors"
	chatclient "gitlab.com/gzavodov/scrambler/pkg/chat/client"
	"gitlab.com/gzavodov/scrambler/pkg/chat/cryptography"
	"log"
	"net"
	"sync"

	"gitlab.com/gzavodov/scrambler/pkg/chat/protocol"
)

type Client struct {
	config *chatclient.Config
	conn   net.Conn

	cmdReader *protocol.CommandReader
	cmdWriter *protocol.CommandWriter

	name string

	cryptoProvider *cryptography.RsaProvider

	error    chan error
	incoming chan protocol.MessageCommand

	subscriberLock *sync.Mutex
	subscribers    []chatclient.Subscriber
}

func NewClient(config *chatclient.Config) *Client {
	return &Client{
		config:         config,
		incoming:       make(chan protocol.MessageCommand),
		error:          make(chan error),
		subscriberLock: &sync.Mutex{},
		subscribers:    make([]chatclient.Subscriber, 0, 2),
	}
}

func (c *Client) Dial(address string) error {
	var err error
	c.conn, err = net.Dial("tcp", address)

	return err
}

func (c *Client) Start() {
	var err error

	if c.config.CryptographyScheme == cryptography.CryptoAlgorithmRSA {
		c.cryptoProvider = cryptography.NewRsaProvider(c.config.Rsa)
	}

	c.NotifySubscribers()

	if err := c.cryptoProvider.Initialize(); err != nil {
		c.error <- err
		return
	}

	c.cmdReader = protocol.NewCommandReader(c.conn, c.cryptoProvider)
	c.cmdWriter = protocol.NewCommandWriter(c.conn, c.cryptoProvider)

	err = c.cmdWriter.Write(
		protocol.NewRSAConnectionRequest(c.cryptoProvider.GetPublicKey(), c.cryptoProvider.GetBitCount(), c.cryptoProvider.GetEnableOAEP()),
	)

	if err != nil {
		c.error <- err
		return
	}

	cmd, err := c.cmdReader.Read()

	if err != nil {
		if errors.Is(err, protocol.ErrConnectionClosed) {
			c.error <- err
		}
		return
	}

	if cmd != nil {
		switch v := cmd.(type) {
		case protocol.SecureConnectionAcknowledgement:
			c.cryptoProvider.SetOppositePublicKey(v.AlgorithmData.(*protocol.RSAProtocolData).PublicKey)
			c.cryptoProvider.SetActive(true)
			c.NotifySubscribers()
		default:
			log.Printf("Unexpected command: %v", v)
		}
	}

	for {
		cmd, err := c.cmdReader.Read()
		if err != nil {
			c.error <- err

			if errors.Is(err, protocol.ErrConnectionClosed) {
				break
			}
		}

		//log.Printf("command read: %v", cmd)

		if cmd != nil {
			switch v := cmd.(type) {
			case protocol.MessageCommand:
				c.incoming <- v
			default:
				log.Printf("Unknown command: %v", v)
			}
		}
	}
}

func (c *Client) Close() {
	c.conn.Close()
}

func (c *Client) Incoming() chan protocol.MessageCommand {
	return c.incoming
}

func (c *Client) Error() chan error {
	return c.error
}

func (c *Client) Send(command interface{}) error {
	return c.cmdWriter.Write(command)
}

func (c *Client) SetName(name string) error {
	err := c.Send(protocol.NameCommand{Name: name})
	if err != nil {
		log.Printf("send name error: %v", err)
	}
	return err
}

func (c *Client) SendMessage(message string) error {
	return c.Send(protocol.SendCommand{Message: message})
}

func (c *Client) GetCryptographyInfo() string {
	if c.cryptoProvider == nil {
		return ""
	}

	return c.cryptoProvider.GetInfo()
}

func (c *Client) AddSubscriber(subscriber chatclient.Subscriber) {
	if subscriber == nil {
		return
	}

	c.subscriberLock.Lock()
	defer c.subscriberLock.Unlock()

	c.subscribers = append(c.subscribers, subscriber)
}

func (c *Client) NotifySubscribers() {
	c.subscriberLock.Lock()
	defer c.subscriberLock.Unlock()

	for _, subscriber := range c.subscribers {
		subscriber.NotifyStateChanged(c)
	}
}
