package tcp

import (
	"errors"
	"gitlab.com/gzavodov/scrambler/pkg/chat/cryptography"
	"log"
	"net"
	"sync"

	"gitlab.com/gzavodov/scrambler/pkg/chat/protocol"
)

type client struct {
	id   uint32
	name string

	conn net.Conn

	cryptoProvider *cryptography.RsaProvider

	reader *protocol.CommandReader
	writer *protocol.CommandWriter
}

type Server struct {
	listener net.Listener

	lastClientID uint32
	clients      []*client
	mutex        *sync.Mutex
}

var (
	ErrUnknownClient = errors.New("unknown client")
)

func NewServer() *Server {
	return &Server{mutex: &sync.Mutex{}}
}

func (s *Server) Listen(address string) error {
	listener, err := net.Listen("tcp", address)
	if err != nil {
		return err
	}

	s.listener = listener
	log.Printf("Listening on %v", address)

	return nil
}

func (s *Server) Close() {
	s.listener.Close()
}

func (s *Server) Start() {
	for {
		// XXX: need a way to break the loop
		conn, err := s.listener.Accept()

		if err != nil {
			log.Print(err)
		} else {
			// handle connection
			client, err := s.accept(conn)
			if err != nil {
				log.Print(err)
				return
			}

			go s.serve(client)
		}
	}
}

func (s *Server) Broadcast(command interface{}) error {
	for _, client := range s.clients {
		// TODO: handle error here?
		client.writer.Write(command)
	}

	return nil
}

func (s *Server) Send(name string, command interface{}) error {
	for _, client := range s.clients {
		if client.name == name {
			return client.writer.Write(command)
		}
	}

	return ErrUnknownClient
}

func (s *Server) accept(conn net.Conn) (*client, error) {
	log.Printf("Accepting connection from %v, total clients: %v", conn.RemoteAddr().String(), len(s.clients)+1)

	s.mutex.Lock()
	defer s.mutex.Unlock()

	s.lastClientID++
	client := &client{id: s.lastClientID, conn: conn}

	s.clients = append(s.clients, client)

	return client, nil
}

func (s *Server) remove(client *client) {
	s.mutex.Lock()
	defer s.mutex.Unlock()

	// remove the connections from clients array
	for i, check := range s.clients {
		if check == client {
			s.clients = append(s.clients[:i], s.clients[i+1:]...)
		}
	}

	log.Printf("Closing connection from %v", client.conn.RemoteAddr().String())
	client.conn.Close()
}

func (s *Server) serve(client *client) {
	defer s.remove(client)

	client.reader = protocol.NewCommandReader(client.conn, nil)
	client.writer = protocol.NewCommandWriter(client.conn, nil)

	for {
		cmd, err := client.reader.Read()
		if err != nil && !errors.Is(err, protocol.ErrConnectionClosed) {
			log.Printf("Read error: %v\n", err)
			continue
		}

		if cmd != nil {
			switch v := cmd.(type) {
			case protocol.SendCommand:
				log.Printf("Client [%d] sent message: %v\n", client.id, v.Message)
				go s.Broadcast(protocol.MessageCommand{
					Message: v.Message,
					Name:    client.name,
				})

			case protocol.NameCommand:
				log.Printf("Client [%d] change his nick: %s\n", client.id, v.Name)
				client.name = v.Name

			case protocol.SecureConnectionRequest:
				if v.AlgorithmName == cryptography.CryptoAlgorithmRSA {
					rsaProtocolData := v.AlgorithmData.(*protocol.RSAProtocolData)

					client.cryptoProvider = cryptography.NewRsaProvider(
						cryptography.NewRsaProviderConfig(
							rsaProtocolData.BitCount,
							rsaProtocolData.EnableOAEP,
						),
					)

					if err := client.cryptoProvider.Initialize(); err != nil {
						log.Printf("Failed to initialize cryptography provider : %v\n", err)
						continue
					}
					client.cryptoProvider.SetOppositePublicKey(rsaProtocolData.PublicKey)

					err := client.writer.Write(
						protocol.NewRSAConnectionAcknowledgement(
							client.cryptoProvider.GetPublicKey(),
							rsaProtocolData.BitCount,
							rsaProtocolData.EnableOAEP,
						),
					)

					if err != nil {
						log.Printf("Failed to send connection acknowledgement: %v\n", err)
						continue
					}

					client.reader.SetDecoder(client.cryptoProvider)
					client.writer.SetEncoder(client.cryptoProvider)

					client.cryptoProvider.SetActive(true)

					log.Printf(
						"Client [%d] esteblished secure connection\nAlgorithm: %s\nBit count: %d\nClient Public Key: [%s]\nPublic Key: [%s]\n",
						client.id,
						client.cryptoProvider.GetAlgorithmName(),
						client.cryptoProvider.GetBitCount(),
						client.cryptoProvider.GetOppositePublicKey().String(),
						client.cryptoProvider.GetPublicKey().String(),
					)

				} else {
					log.Printf("Unsupported cryptography algorithm: %v\n", v.AlgorithmName)
				}
			}
		}

		if errors.Is(err, protocol.ErrConnectionClosed) {
			break
		}
	}
}
