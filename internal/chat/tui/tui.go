package tui

import (
	"errors"
	"fmt"
	"github.com/marcusolsson/tui-go"
	"gitlab.com/gzavodov/scrambler/pkg/chat/protocol"
	"log"

	"gitlab.com/gzavodov/scrambler/pkg/chat/client"
)

func StartUi(c client.Client) {
	loginView := NewLoginView()
	chatView := NewChatView(c)

	ui, err := tui.New(loginView)
	if err != nil {
		panic(err)
	}

	quit := func() { ui.Quit() }

	ui.SetKeybinding("Esc", quit)
	ui.SetKeybinding("Ctrl+c", quit)

	loginView.OnLogin(func(username string) {
		c.SetName(username)
		ui.SetWidget(chatView)
	})

	chatView.OnSubmit(func(msg string) {
		c.SendMessage(msg)
	})

	go func() {
		for {
			select {
			case err := <-c.Error():
				if errors.Is(err, protocol.ErrConnectionClosed) {
					ui.Update(func() {
						chatView.AddMessage("Connection closed connection from server.")
					})
				} else {
					chatView.AddMessage(fmt.Sprintf("Error: %s", err.Error()))
					log.Fatal(err)
				}
			case msg := <-c.Incoming():
				// we need to make the change via ui update to make sure the ui is repaint correctly
				ui.Update(func() {
					chatView.AddMessage(fmt.Sprintf("<%s>: %s", msg.Name, msg.Message))
				})
			}
		}
	}()

	if err := ui.Run(); err != nil {
		panic(err)
	}
}
