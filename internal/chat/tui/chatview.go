package tui

import (
	"github.com/marcusolsson/tui-go"
	"gitlab.com/gzavodov/scrambler/pkg/chat/client"
)

type SubmitMessageHandler func(string)

type ChatView struct {
	client client.Client
	tui.Box
	history      *tui.Box
	frame        *tui.Box
	cryptography *tui.Label

	onSubmit SubmitMessageHandler
}

func NewChatView(client client.Client) *ChatView {
	view := &ChatView{client: client}

	view.cryptography = tui.NewLabel("")
	cryptographyBox := tui.NewHBox(
		tui.NewLabel("Cryptography:"),
		tui.NewPadder(1, 0, view.cryptography),
		tui.NewSpacer(),
	)

	cryptographyBox.SetBorder(true)
	cryptographyBox.SetSizePolicy(tui.Expanding, tui.Maximum)

	// ref: https://github.com/marcusolsson/tui-go/blob/master/example/chat/main.go
	view.history = tui.NewVBox()

	historyScroll := tui.NewScrollArea(view.history)
	historyScroll.SetAutoscrollToBottom(true)

	historyBox := tui.NewVBox(historyScroll)
	historyBox.SetBorder(true)

	input := tui.NewEntry()
	input.SetFocused(true)
	input.SetSizePolicy(tui.Expanding, tui.Maximum)

	input.OnSubmit(func(e *tui.Entry) {
		if e.Text() != "" {
			if view.onSubmit != nil {
				view.onSubmit(e.Text())
			}

			e.SetText("")
		}
	})

	inputBox := tui.NewHBox(input)
	inputBox.SetBorder(true)
	inputBox.SetSizePolicy(tui.Expanding, tui.Maximum)

	view.frame = tui.NewVBox(
		cryptographyBox,
		historyBox,
		inputBox,
		tui.NewSpacer(),
	)

	view.frame.SetBorder(false)
	view.Append(view.frame)

	view.Refresh()
	view.client.AddSubscriber(view)

	return view
}

func (c *ChatView) OnSubmit(handler SubmitMessageHandler) {
	c.onSubmit = handler
}

func (c *ChatView) AddMessage(msg string) {
	c.history.Append(
		tui.NewHBox(
			tui.NewLabel(msg),
		),
	)
}

func (c *ChatView) Refresh() {
	cryptographyState := c.client.GetCryptographyInfo()
	if len(cryptographyState) == 0 {
		cryptographyState = "N/A"
	}

	c.cryptography.SetText(cryptographyState)
}

func (c *ChatView) NotifyStateChanged(observer client.Observer) {
	c.Refresh()
}
